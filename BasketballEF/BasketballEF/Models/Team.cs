﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BasketballEF.Enums;
using BasketballEF.Interfaces;

namespace BasketballEF.Models
{
    /// <summary>
    /// Basketball team entity.
    /// </summary>
    public class Team : IEntity<int>
    {
        /// <summary>
        /// Identity.
        /// </summary>
        public int Id { get; set; }

        [ForeignKey("Coach")]
        public int CoachId { get; set; }

        /// <summary>
        /// Basketball team name.
        /// </summary>
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// Origin city of the team HQ.
        /// </summary>
        [Required]
        public string City { get; set; }

        /// <summary>
        /// Western or Eastern conference.
        /// </summary>
        public Conference Conference { get; set; }

        /// <summary>
        /// Year the team was created on.
        /// </summary>
        public int FundYear { get; set; }

        /// <summary>
        /// Number of wins in the current season.
        /// </summary>
        public int WinCount { get; set; }

        /// <summary>
        /// Number of losses in the current season.
        /// </summary>
        public int LoseCount { get; set; }

        public Coach Coach { get; set; }
        public List<Player> Players { get; set; }
    }
}
