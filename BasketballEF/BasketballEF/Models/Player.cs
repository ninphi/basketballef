﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BasketballEF.Enums;

namespace BasketballEF.Models
{
    public class Player : Person
    {
        [ForeignKey("Team")]
        public int TeamId { get; set; }
        public float Height { get; set; }
        public float Weight { get; set; }
        public int JerseyNumber { get; set; }
        public Position Position { get; set; }

        public Team Team { get; set; }
    }
}
