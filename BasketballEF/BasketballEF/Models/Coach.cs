﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BasketballEF.Models
{
    public class Coach : Person
    {
        [ForeignKey("Team")]
        public int TeamId { get; set; }
        public int SeasonsCount { get; set; }

        public Team Team { get; set; }
    }
}
