﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BasketballEF.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace BasketballEF
{
    public class AppDbContext : DbContext
    {
        public DbSet<Team> Teams { get; set; }
        public DbSet<Player> Players { get; set; }
        public DbSet<Coach> Coaches { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder builder)
        {
            var config = new ConfigurationBuilder()
                .SetBasePath(@"C:\Users\melma\source\repos\basketballef\BasketballEF\BasketballEF")
                .AddJsonFile("appsettings.json")
                .Build();

            var connectionString = config.GetConnectionString("DefaultConnection");
            builder.UseSqlServer(connectionString);
        }

        /// <summary>
        /// Model Configs
        /// </summary>
        /// <param name="builder"></param>
        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<Team>()
                .HasIndex(i => i.Name)
                .IsUnique();

            builder.Entity<Player>()
                .HasIndex(i => new { i.TeamId, i.JerseyNumber })
                .IsUnique();

            builder.Entity<Team>()
                .HasOne(x => x.Coach)
                .WithOne(x => x.Team)
                .HasForeignKey<Coach>(x => x.TeamId)
                .OnDelete(DeleteBehavior.NoAction);

            builder.Entity<Team>()
                .HasMany(x => x.Players)
                .WithOne(x => x.Team)
                .HasForeignKey(x => x.TeamId)
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
